package dev.hack5.isotope

import com.soywiz.klogger.Logger
import dev.hack5.isotope.modulemanager.Context
import dev.hack5.isotope.modulemanager.MemoryConfigurationManagerImpl
import dev.hack5.isotope.modulemanager.ModuleManager
import dev.hack5.isotope.modulemanager.TranslationManagerDefaultImpl
import dev.hack5.isotope.modules.Ping
import dev.hack5.isotope.modules.ReactionEvent
import dev.hack5.isotope.modules.Reactions
import io.ktor.client.*
import io.ktor.client.engine.java.*
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import net.folivo.trixnity.client.rest.MatrixClient
import net.folivo.trixnity.client.rest.MatrixClientProperties
import net.folivo.trixnity.core.model.events.Event
import net.folivo.trixnity.core.model.events.m.room.MessageEventContent
import net.folivo.trixnity.core.serialization.event.EventContentSerializerMapping
import java.io.File


fun main() = runBlocking {
    Logger.defaultLevel = Logger.Level.TRACE
    val roomEventContentSerializerMapping = setOf(
        EventContentSerializerMapping("m.reaction", ReactionEvent::class, ReactionEvent.serializer()),
    )
    val matrixClient = MatrixClient(
            HttpClient(Java),
            MatrixClientProperties(
                MatrixClientProperties.MatrixHomeServerProperties("matrix.org"),
                File("token.txt").readText().trim() // TODO
            ),
            customRoomEventContentSerializers = roomEventContentSerializerMapping
    )
    val me = matrixClient.user.whoAmI()
    val moduleManager = ModuleManager()
    val translationManager = TranslationManagerDefaultImpl()
    val configurationManager = MemoryConfigurationManagerImpl()
    val context = Context(moduleManager, matrixClient, translationManager, configurationManager)
    moduleManager.addModule(Reactions(context))
    moduleManager.addModule(Ping(context))
    val listener = matrixClient.sync.events<MessageEventContent.TextMessageEventContent>()
        .onEach {
            val roomEvent = (it as? Event.RoomEvent) ?: return@onEach
            val event = it.content
            if (roomEvent.sender == me) {
                println(event.body)
                try {
                    moduleManager.execute(it)
                } catch (e: Exception) {
                    if (e is CancellationException)
                        throw e
                    e.printStackTrace()
                }
            }
        }
        .catch {
            it.printStackTrace()
        }
        .launchIn(this)
    matrixClient.sync.start()
    withContext(Dispatchers.IO) {
        println("ready")
        readLine()
    }
    println("done")
    matrixClient.sync.stop()
    listener.cancel()
}