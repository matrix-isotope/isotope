package dev.hack5.isotope.modules

import dev.hack5.isotope.parser.argparse.command
import dev.hack5.isotope.translation.translatable
import dev.hack5.isotope.utils.id
import dev.hack5.isotope.utils.roomId
import net.folivo.trixnity.core.model.MatrixId
import kotlin.system.measureNanoTime

val Ping = command("Ping", "ping") {
    command("get") {
        deletable = false
        targeted = false

        val calculating by translatable("Calculating")
        val calculated by translatable("Calculation complete")
        val calculatedValue by translatable("%.2f ms")

        execute {
            parse()

            val reaction: MatrixId.EventId
            val time = measureNanoTime {
                reaction = matrixClient.room.sendRoomEvent(event.roomId!!, ReactionEvent(event.id!!, calculating))
            }
            matrixClient.room.sendRedactEvent(event.roomId!!, reaction, calculated)
            matrixClient.room.sendRoomEvent(event.roomId!!, ReactionEvent(event.id!!, calculatedValue.format(translationManager.locale, time / 1000000f)))
        }
    }
}