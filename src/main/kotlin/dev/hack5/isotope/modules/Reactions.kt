package dev.hack5.isotope.modules

import dev.hack5.isotope.parser.argparse.command
import kotlinx.cli.ArgType
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import net.folivo.trixnity.core.model.MatrixId
import net.folivo.trixnity.core.model.events.RoomEventContent

@Serializable
data class ReactionEvent(@SerialName("m.relates_to") val content: ReactionEventContent? = null) : RoomEventContent

fun ReactionEvent(eventId: MatrixId.EventId, key: String, relType: String = "m.annotation"): ReactionEvent {
    return ReactionEvent(ReactionEventContent(eventId, key, relType))
}

@Serializable
data class ReactionEventContent(@SerialName("event_id") val eventId: MatrixId.EventId, val key: String, @SerialName("rel_type") val relType: String? = null)

val Reactions = command("Reactions", "reactions") {
    command("add") {
        execute {
            val reaction by argument(ArgType.String)
            parse()

            val target = getTarget()!!
            matrixClient.room.sendRoomEvent(target.first, ReactionEvent(target.second, reaction))
            delete()
        }
    }
}
