package dev.hack5.isotope.translation

fun TranslationContext.translatable(default: String) = TranslationDelegate(this, default)