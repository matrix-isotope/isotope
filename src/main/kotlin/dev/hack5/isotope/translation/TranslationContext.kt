package dev.hack5.isotope.translation

import dev.hack5.isotope.modulemanager.TranslationManager

interface TranslationContext {
    val name: String
    val translationManager: TranslationManager
}