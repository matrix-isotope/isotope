package dev.hack5.isotope.translation

import kotlin.reflect.KProperty

class TranslationDelegate(private val translationContext: TranslationContext, private val default: String) {
    operator fun getValue(thisRef: Any?, prop: KProperty<*>): String {
        return translationContext.translationManager.translate(translationContext.name + ".." + prop.name, default)
    }
}
