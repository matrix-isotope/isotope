package dev.hack5.isotope.configuration

import dev.hack5.isotope.modulemanager.ConfigurationManager

interface ConfigurationContext {
    val configurationManager: ConfigurationManager

    val name: String
}