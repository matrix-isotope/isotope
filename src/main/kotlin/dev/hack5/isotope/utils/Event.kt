package dev.hack5.isotope.utils

import net.folivo.trixnity.core.model.MatrixId
import net.folivo.trixnity.core.model.events.Event

val Event<*>.roomId: MatrixId.RoomId? get() =
    when (this) {
        is Event.BasicEvent -> null
        is Event.RoomEvent -> roomId
        is Event.StateEvent -> roomId
        is Event.StrippedStateEvent -> roomId
    }

val Event<*>.id: MatrixId.EventId? get() =
    when (this) {
        is Event.BasicEvent -> null
        is Event.RoomEvent -> id
        is Event.StateEvent -> id
        is Event.StrippedStateEvent -> null
    }
