package dev.hack5.isotope.parser.argparse

import dev.hack5.isotope.configuration.ConfigurationContext
import dev.hack5.isotope.modulemanager.*
import dev.hack5.isotope.translation.TranslationContext
import dev.hack5.isotope.utils.id
import dev.hack5.isotope.utils.roomId
import kotlinx.cli.ArgParser
import kotlinx.cli.ArgType
import kotlinx.cli.default
import net.folivo.trixnity.core.model.MatrixId
import net.folivo.trixnity.core.model.events.Event
import net.folivo.trixnity.core.model.events.m.room.MessageEventContent
import java.io.StreamTokenizer
import java.io.StringReader


class Target(parser: ArgParser) {
    val reply by parser.option(ArgType.Boolean, "reply", "r").default(false)
    val eventId by parser.option(ArgType.String, "event")
    val roomId by parser.option(ArgType.String, "room")
}


class Delete(parser: ArgParser) {
    val delete by parser.option(ArgType.Boolean, "delete", "d").default(false)
}

interface ArgParserTree : CommandTree<ArgParserTree> {
    suspend fun execute(event: Event<*>, argumentsArray: Array<String>, offset: Int)
}


class ArgParserCommandContext(
    val event: Event<*>,
    val context: Context,
    private val arguments: Array<String>,
    private val offset: Int, name: String,
    deletable: Boolean,
    targeted: Boolean
) : CommandContext, ArgParser(name) {
    init {
        outputAndTerminate = { message, exitCode -> throw ParseError(message, exitCode) }
    }
    val moduleManager get() = context.moduleManager
    val matrixClient get() = context.matrixClient
    val translationManager get() = context.translationManager
    val configurationManager get() = context.configurationManager

    override val roomEvent get() = event as? Event.RoomEvent
    override val eventContent get() = event.content
    override suspend fun getTarget(): Pair<MatrixId.RoomId, MatrixId.EventId>? {
        requireNotNull(target) { "cannot getTarget() when targeted was set to false" }
        return when {
            target.reply -> {
                require(target.eventId == null && target.roomId == null) { "Cannot specify reply with event" }
                return null // TODO
            }
            target.eventId != null -> {
                if (target.roomId != null)
                    MatrixId.RoomId(target.roomId!!) to MatrixId.EventId(target.eventId!!)
                else
                    event.roomId?.let { it to MatrixId.EventId(target.eventId!!) }
            }
            target.roomId != null -> {
                throw IllegalArgumentException("Cannot specify room without event")
            }
            else -> (event.roomId ?: return null) to (event.id ?: return null)
        }
    }

    private val target = if (targeted) Target(this) else null
    private val delete = if (deletable) Delete(this) else null

    // TODO interface?
    suspend fun delete(): Boolean {
        if (delete?.delete != true)
            return false
        matrixClient.room.sendRedactEvent(event.roomId ?: return false, event.id ?: return false)
        return true
    }

    fun parse() {
        println(arguments.contentToString())
        parse(arguments.sliceArray(offset .. arguments.lastIndex))
    }
}

open class CommandLeafImpl(private val block: suspend (ArgParserCommandContext) -> Unit, val name: String, private val context: Context, private val deletable: Boolean, private val targeted: Boolean) : CommandLeaf<ArgParserTree>, ArgParserTree {
    override val subcommands = null

    override suspend fun execute(event: Event<*>, argumentsArray: Array<String>, offset: Int) {
        execute(ArgParserCommandContext(event, context, argumentsArray, offset, name, deletable, targeted))
    }

    private suspend fun execute(context: ArgParserCommandContext) = block(context)
}

open class CommandHolderImpl(override val subcommands: Map<String, ArgParserTree>) : CommandHolder<ArgParserTree>, ArgParserTree {
    override suspend fun execute(event: Event<*>, argumentsArray: Array<String>, offset: Int) {
        (subcommands[argumentsArray[offset]] ?: return).execute(event, argumentsArray, offset + 1)
    }
}

class CommandBuilder(private val names: List<String>, val context: Context) : TranslationContext, ConfigurationContext {
    private val commands = mutableMapOf<String, ArgParserTree>()
    private var execute: (suspend (ArgParserCommandContext) -> Unit)? = null

    override val name
        get() = names.joinToString(".")
    override val translationManager
        get() = context.translationManager
    override val configurationManager: ConfigurationManager
        get() = context.configurationManager

    var deletable: Boolean? = null
        set(value) {
            require(commands.isEmpty()) { "A command cannot have both subcommands and set deletable" }
            field = value
        }
    var targeted: Boolean? = null
        set(value) {
            require(commands.isEmpty()) { "A command cannot have both subcommands and set targeted" }
            field = value
        }

    fun command(name: String, block: CommandBuilder.() -> Unit) {
        require(execute == null) { "A command cannot have both subcommands and an execute block" }

        commands.putIfAbsent(name, CommandBuilder(names + name, context).apply(block).build())?.let {
            error("Commands cannot be duplicated")
        }
    }

    fun execute(block: suspend ArgParserCommandContext.() -> Unit) {
        require(commands.isEmpty()) { "A command cannot have both subcommands and an execute block" }
        execute = block
    }

    fun build(): ArgParserTree {
        if (execute == null && commands.isEmpty())
            error("A command must have either subcommands or an execute block")
        execute?.let {
            return CommandLeafImpl(it, names.last(), context, deletable ?: true, targeted ?: true)
        }
        return CommandHolderImpl(commands)
    }
}

class ModuleBuilder(val name: String, val context: Context) {
    private val commands = mutableMapOf<String, ArgParserTree>()

    fun command(name: String, block: CommandBuilder.() -> Unit) {
        commands.putIfAbsent(name, CommandBuilder(listOf(this.name, name), context).apply(block).build())?.let {
            error("Commands cannot be duplicated")
        }
    }

    fun build(): ModuleImpl {
        return ModuleImpl(name, commands)
    }
}


fun command(moduleName: String, name: String, block: CommandBuilder.() -> Unit): (Context) -> Module<*> {
    return module(moduleName) {
        command(name) {
            block()
        }
    }
}


class ModuleImpl(override val name: String, override val commands: Map<String, ArgParserTree>) : Module<ArgParserTree> {
    override suspend fun execute(event: Event<*>) {
        (event.content as? MessageEventContent.TextMessageEventContent)?.let {
            val tokenizer = StreamTokenizer(StringReader(it.body)).apply {
                resetSyntax()
                wordChars(0, 0xFF)
                whitespaceChars(0, ' '.code)
                quoteChar('"'.code)
                quoteChar('\''.code)
            }
            val argumentsArray = mutableListOf<String>()

            @Suppress("BlockingMethodInNonBlockingContext")
            while (tokenizer.nextToken() != StreamTokenizer.TT_EOF) {
                assert (tokenizer.ttype == StreamTokenizer.TT_WORD || tokenizer.ttype >= 0 && tokenizer.sval != null)
                argumentsArray += tokenizer.sval
            }

            (commands[argumentsArray.first()] ?: return).execute(event, argumentsArray.toTypedArray(), 1)
        }
    }
}

fun module(name: String, block: ModuleBuilder.() -> Unit): (Context) -> Module<*> {
    return { context -> ModuleBuilder(name, context).apply(block).build() }
}

class ParseError(message: String, val exitCode: Int) : Exception(message) {

}