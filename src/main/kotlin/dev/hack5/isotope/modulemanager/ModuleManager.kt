package dev.hack5.isotope.modulemanager

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import net.folivo.trixnity.client.rest.MatrixClient
import net.folivo.trixnity.core.model.MatrixId
import net.folivo.trixnity.core.model.events.Event
import net.folivo.trixnity.core.model.events.EventContent
import java.util.*

interface CommandTree<T : CommandTree<T>> {
    val subcommands: Map<String, CommandTree<T>>?
}

interface Module<T : CommandTree<T>> {
    val name: String
    val commands: Map<String, T>

    suspend fun execute(event: Event<*>)
}


interface CommandLeaf<T : CommandTree<T>> : CommandTree<T> {
    override val subcommands: Nothing?
}

interface CommandHolder<T : CommandTree<T>> : CommandTree<T> {
    override val subcommands: Map<String, T>
}

interface CommandContext {
    val roomEvent: Event.RoomEvent<*>?
    val eventContent: EventContent

    suspend fun getTarget(): Pair<MatrixId.RoomId, MatrixId.EventId>?
}

class ModuleManager {
    private val lock = Mutex()

    var modules = listOf<Module<*>>()
        private set

    suspend fun addModule(module: Module<*>) {
        lock.withLock {
            modules = modules + module
        }
    }

    suspend fun execute(event: Event<*>) {
        modules.forEach {
            it.execute(event)
        }
    }
}

data class Context(
    val moduleManager: ModuleManager,
    val matrixClient: MatrixClient,
    val translationManager: TranslationManager,
    val configurationManager: ConfigurationManager,
)

interface TranslationManager {
    fun translate(name: String): String?
    fun translate(name: String, default: String): String

    val locale: Locale
}

class TranslationManagerDefaultImpl : TranslationManager {
    override fun translate(name: String): String? {
        return null
    }

    override fun translate(name: String, default: String): String {
        return default
    }

    override val locale = Locale.ENGLISH!!
}


interface ConfigurationManager {
    suspend fun <T>get(key: String): T
    suspend fun <T>set(key: String, value: T)
    suspend fun del(key: String)
}

class MemoryConfigurationManagerImpl : ConfigurationManager {
    val lock = Mutex()
    val data = mutableMapOf<String, Any?>()

    override suspend fun <T> get(key: String): T {
        lock.withLock {
            @Suppress("UNCHECKED_CAST")
            return data[key] as T
        }
    }

    override suspend fun <T> set(key: String, value: T) {
        lock.withLock {
            data[key] = value
        }
    }

    override suspend fun del(key: String) {
        lock.withLock {
            data.remove(key)
        }
    }
}
